# Vanity Ethereum Key Generator

&nbsp;

## __:black_nib: Notes:__

__❍__ Currently unfinished...a wip if you will.

&nbsp;

## __:page_with_curl: Instructions:__

**1)** Clone the repo:

_**`❍ git clone https://gitlab.com/gskapka/eth-keygen-rust.git`**_

**2)** Enter app dir and build it:

_**`❍ cd eth-keygen-rust/app && cargo build`**_

**3)** Enter dir & run it:

_**`❍ cd target/debug && ./eth-keygen-rust`**_


&nbsp;

## __:clipboard: To Do List:__

:black_square_button: Make as CLI w/ Docopt

:black_square_button: Init once then check if key exists before overwriting

:black_square_button: All custom message passing in for signing

